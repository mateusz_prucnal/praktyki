package pl.clarite.UserService.service;
import pl.clarite.UserService.exception.MyException;
import pl.clarite.UserService.model.Person;
import pl.clarite.UserService.model.dto.PersonDto;
import pl.clarite.UserService.model.mapper.ModelMapper;
import pl.clarite.UserService.repository.PersonRepository;

import javax.validation.constraints.Null;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@org.springframework.stereotype.Service
public class PersonService {

    private static ModelMapper modelMapper;
    private static PersonRepository personRepository;

    public PersonService(ModelMapper modelMapper, PersonRepository personRepository) {
        this.modelMapper = modelMapper;
        this.personRepository = personRepository;
    }

    public PersonDto save(PersonDto personDto) {
        return modelMapper.fromPersonToPersonDto(personRepository.save(modelMapper.fromPersonDtoToPerson(personDto)));
    }

    public PersonDto findById(Long id) throws MyException {
        return modelMapper.fromPersonToPersonDto(personRepository.findById(id).orElseThrow(()-> new MyException("Can't find person with id: "+id)));
    }

    public List<PersonDto> findAll() {
        return personRepository.findAll()
                .stream()
                .map(modelMapper::fromPersonToPersonDto)
                .collect(Collectors.toList());
    }

    public static PersonDto findPersonWithMaxAge() throws MyException {
        return modelMapper.fromPersonToPersonDto(personRepository.findAll().stream().max(Comparator.comparing(Person::getAge).thenComparing(Person::getAge)).orElseThrow(NoSuchElementException::new));
    }

    public static PersonDto findPersonWithMinAge() throws MyException {
        return modelMapper.fromPersonToPersonDto(personRepository.findAll().stream().min(Comparator.comparing(Person::getAge).thenComparing(Person::getAge)).orElseThrow(NoSuchElementException::new));
    }
    public static PersonDto DeletePerson(Long id) throws MyException {
        Person person = personRepository.findById(id).orElseThrow(() -> new MyException("Can't find person with such id"+ id));
        personRepository.delete(person);
        return modelMapper.fromPersonToPersonDto(person);
    }

}