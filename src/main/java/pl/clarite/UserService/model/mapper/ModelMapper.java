package pl.clarite.UserService.model.mapper;

import org.springframework.stereotype.Component;
import pl.clarite.UserService.model.Person;
import pl.clarite.UserService.model.dto.PersonDto;



@Component
public class ModelMapper {

    public Person fromPersonDtoToPerson(PersonDto personDto) {
        return personDto == null ? null : Person.builder()
                .id(personDto.getId())
                .firstname(personDto.getFirstname())
                .lastname(personDto.getLastname())
                .username(personDto.getUsername())
                .age(personDto.getAge())
                .build();
    }

    public PersonDto fromPersonToPersonDto(Person person) {
        return person == null ? null : PersonDto.builder()
                .id(person.getId())
                .firstname(person.getFirstname())
                .lastname(person.getLastname())
                .username(person.getUsername())
                .age(person.getAge())
                .build();
    }
}

