package pl.clarite.UserService.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Pattern;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "people")
public class Person {
    @Id
    @GeneratedValue
    @NotNull
   private Long id;
    @Length(min = 3)
   private String firstname;
    @Pattern(regexp = "[A-Z][a-z]+")
   private String lastname;
    @Pattern(regexp = "[A-Z][a-z]+")
   private String username;
   private Integer age;
}


