package pl.clarite.UserService.exception;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.ControllerAdvice;

import java.text.MessageFormat;
import java.time.LocalDateTime;


@AllArgsConstructor
public class MyException extends RuntimeException {
private String message;

    /*@Override
    public String getMessage() {
        return MessageFormat.format("[{0}]: {1}",LocalDateTime.now(),message);
    }*/

    @Override
    public String getMessage() {
        return message;
    }
}


