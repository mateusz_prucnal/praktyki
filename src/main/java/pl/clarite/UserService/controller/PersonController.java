package pl.clarite.UserService.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.clarite.UserService.exception.MyException;
import pl.clarite.UserService.model.Person;
import pl.clarite.UserService.model.dto.PersonDto;
import pl.clarite.UserService.model.mapper.ModelMapper;
import pl.clarite.UserService.service.PersonService;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
public class PersonController {

    private PersonService personService;
    private ModelMapper modelMapper;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("/person/{id}")
    public ResponseEntity<PersonDto> findById(@PathVariable Long id) {
       /* try {
            return new ResponseEntity<>(personService.findById(id), HttpStatus.OK);
        } catch (MyException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }*/
        return new ResponseEntity<>(personService.findById(id), HttpStatus.OK);
    }

    @GetMapping("/people")
    public ResponseEntity<List<PersonDto>> findAll() {
        return new ResponseEntity<>(personService.findAll(), HttpStatus.OK);
    }

    @PostMapping("/person")
    public ResponseEntity<PersonDto> save(@RequestBody PersonDto personDto) {
        return new ResponseEntity<>(personService.save(personDto), HttpStatus.CREATED);
    }

    @GetMapping("/Person/max")
    public ResponseEntity<PersonDto> findPersonWithMaxAge() {

        /*try {
            return new ResponseEntity<>(PersonService.findPersonWithMaxAge(), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }*/
        return new ResponseEntity<>(PersonService.findPersonWithMaxAge(), HttpStatus.OK);

    }

    @GetMapping("/Person/min")
    public ResponseEntity<PersonDto> findPersonWithMinAge() {
        /*try {
            return new ResponseEntity<>(PersonService.findPersonWithMinAge(), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }*/
        return new ResponseEntity<>(PersonService.findPersonWithMinAge(), HttpStatus.OK);
    }

    @DeleteMapping("/Person/delete/{id}")
    public ResponseEntity<PersonDto> DeletePerson(@PathVariable Long id) {
        /*try {
            return new ResponseEntity<>(PersonService.DeletePerson(id), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }*/
        return new ResponseEntity<>(PersonService.DeletePerson(id), HttpStatus.OK);
    }
}


